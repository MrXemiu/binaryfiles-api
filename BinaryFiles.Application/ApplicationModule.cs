﻿using System;
using Autofac;

namespace BinaryFiles.Application
{
    public class ApplicationModule: Module
    {
        #region Overrides of Module


        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ApplicationModule).Assembly;
            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.Name.EndsWith("Provider")
                               || type.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
        }


        #endregion
    }
}
