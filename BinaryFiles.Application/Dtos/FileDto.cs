﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace BinaryFiles.Application.Dtos
{
    public class FileDto
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public long SizeInBytes { get; set; }
        [JsonIgnore]
        public Stream DataStream { get; set; }
    }
}
