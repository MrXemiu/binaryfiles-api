﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryFiles.Application.Dtos;
using BinaryFiles.Application.Util;
using BinaryFiles.Domain.Services;
using Microsoft.AspNetCore.Http;

namespace BinaryFiles.Application.Services
{
    public class FileApplicationService : IFileApplicationService
    {
        private readonly IFileService _fileService;

        public FileApplicationService(IFileService fileService)
        {
            _fileService = fileService;
        }


        #region Implementation of IFileApplicationService


        public async Task Save(IFormFile formFile)
        {
            var fileName = formFile.FileName;
            using var fileStream = formFile.OpenReadStream();
            await _fileService.AddFile(fileName, fileStream);
        }

        public async Task<FileDto> Get(int fileId)
        {
            var mimeTypeLookup = new MimeTypeLookup();
            var (fileRecord, fileStream) = await _fileService.GetFileStream(fileId);
            
            if (fileRecord == null) return null;

            var fileDto = new FileDto
            {
                Id = fileRecord.Id
                , FileName = fileRecord.OriginalName
                , MimeType = mimeTypeLookup.GetMimeType(fileRecord.OriginalName)
                , SizeInBytes = fileRecord.SizeInBytes
                , DataStream = fileStream
            };
            return fileDto;
        }

        public async Task<IEnumerable<FileDto>> GetAllForUser(int userId)
        {
            var mimeTypeLookup = new MimeTypeLookup();
            var fileRecords = await _fileService.GetUserFiles(userId);
            var fileDtos = fileRecords.Select(file => new FileDto
            {
                Id = file.Id
                , FileName = file.OriginalName
                , MimeType = mimeTypeLookup.GetMimeType(file.OriginalName)
                , SizeInBytes = file.SizeInBytes
            });
            return fileDtos;
        }

        public async Task<bool> Delete(int fileId)
        {
            try
            {
                await _fileService.DeleteFile(fileId);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }

            return false;
        }


        #endregion
    }
}