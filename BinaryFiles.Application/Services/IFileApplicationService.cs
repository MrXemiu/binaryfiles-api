﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryFiles.Application.Dtos;
using Microsoft.AspNetCore.Http;

namespace BinaryFiles.Application.Services
{
    public interface IFileApplicationService
    {
        Task Save(IFormFile formFile);
        Task<FileDto> Get(int fileId);
        Task<IEnumerable<FileDto>> GetAllForUser(int userId);
        Task<bool> Delete(int fileId);
    }
}