﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using BinaryFiles.DiskStorage.Services;

namespace BinaryFiles.DiskStorage
{
    public class DiskStorageModule : Module
    {
        #region Overrides of Module


        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(DiskStorageModule).Assembly;
            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.Name.EndsWith("Provider")
                               || type.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
        }


        #endregion
    }
}
