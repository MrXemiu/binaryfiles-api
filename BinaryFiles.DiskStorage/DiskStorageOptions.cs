﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryFiles.DiskStorage
{
    public class DiskStorageOptions
    {
        public const string DiskStorage = "DiskStorage";

        public string RootDirectory { get; set; }
    }
}
