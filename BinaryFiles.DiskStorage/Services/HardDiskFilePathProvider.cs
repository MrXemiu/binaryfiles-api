﻿using System;
using System.IO;
using System.Threading.Tasks;
using BinaryFiles.Domain.Exceptions;
using BinaryFiles.Domain.Services;
using Microsoft.Extensions.Options;

namespace BinaryFiles.DiskStorage.Services
{
    public class HardDiskFilePathProvider : IFilePathProvider
    {
        private readonly IContextProvider _contextProvider;
        private readonly IUserService _userService;
        private readonly DiskStorageOptions _options;

        public HardDiskFilePathProvider(IOptions<DiskStorageOptions> options, IContextProvider contextProvider,
            IUserService userService)
        {
            _contextProvider = contextProvider;
            _userService = userService;
            _options = options?.Value;
        }

        public async Task<Uri> GetPath(string fileName, string subDirectory = null)
        {
            var currentUserName = await _contextProvider.GetCurrentUserName();
            var currentUser = await _userService.GetUser(currentUserName);
            var path = Path.Combine(_options.RootDirectory, currentUser.Id.ToString(), subDirectory ?? string.Empty,
                fileName);

            if (Uri.TryCreate(path, UriKind.Absolute, out var uri))
            {
                return uri;
            }

            throw new InvalidFilePathException($"Path '{path}' is invalid.");
        }
    }
}