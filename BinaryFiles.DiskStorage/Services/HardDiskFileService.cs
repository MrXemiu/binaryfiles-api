﻿using System;
using System.IO;
using System.Threading.Tasks;
using BinaryFiles.Domain.Exceptions;
using BinaryFiles.Domain.Services;

namespace BinaryFiles.DiskStorage.Services
{
    public class HardDiskFileService : IBinaryService
    {
        private readonly IFilePathProvider _filePathProvider;

        public HardDiskFileService(IFilePathProvider filePathProvider)
        {
            _filePathProvider = filePathProvider;
        }

        #region Implementation of IBinaryService

        public Stream GetBinaryFileStream(Uri filePath)
        {
            if(!filePath.IsFile) throw new InvalidFilePathException($"Path {filePath.LocalPath} is not a file.");

            var localPath = filePath.LocalPath;
            return new FileStream(localPath, FileMode.Open);
        }

        public async Task<Uri> SaveBinaryFileStream(string fileName, Stream inputStream, string subDirectory = null)
        {
            var uri = await _filePathProvider.GetPath(fileName, subDirectory);
            if (!uri.IsFile) throw new InvalidFilePathException($"Path {uri.LocalPath} is not a file.");

            var localPath = uri.LocalPath;
            var directory = Path.GetDirectoryName(localPath);

            if (directory == null) throw new NullReferenceException(nameof(directory));

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using var stream = new FileStream(localPath, FileMode.Create);
            await inputStream.CopyToAsync(stream);

            return uri;
        }

        public void Delete(Uri filePath)
        {
            if (!filePath.IsFile) throw new InvalidFilePathException($"Path {filePath.LocalPath} is not a file.");

            var localPath = filePath.LocalPath;
            File.Delete(localPath);
        }

        #endregion
    }
}