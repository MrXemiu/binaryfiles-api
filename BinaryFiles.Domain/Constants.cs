﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryFiles.Domain
{
    public enum FileAccessType
    {
        Read,
        ReadWrite
    }
    public static class Constants
    {
        public const string DefaultUserName = "Anonymous";
    }
}
