﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.DataAccess
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        TEntity GetById(int id);
        Task<TEntity> GetByIdAsync(int id);
        IEnumerable<TEntity> List();
        Task<IEnumerable<TEntity>> ListAsync();
        IEnumerable<TEntity> List(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> ListAsync(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        Task AddAsync(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}
