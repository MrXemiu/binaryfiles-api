﻿using System;
using System.Collections.Generic;
using System.Text;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.DataAccess
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
