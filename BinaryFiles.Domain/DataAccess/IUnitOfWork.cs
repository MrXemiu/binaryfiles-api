﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;

        int Commit(bool autoHistory = false);
        Task<int> CommitAsync(bool autoHistory = false);
    }
}
