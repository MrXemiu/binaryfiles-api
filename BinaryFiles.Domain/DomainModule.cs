﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;

namespace BinaryFiles.Domain
{
    public class DomainModule: Module
    {
        #region Overrides of Module


        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(DomainModule).Assembly;
            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.Name.EndsWith("Provider")
                               || type.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
        }


        #endregion
    }
}
