﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryFiles.Domain.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
