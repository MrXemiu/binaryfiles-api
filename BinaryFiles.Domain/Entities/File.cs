﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BinaryFiles.Domain.Entities
{
    public class File : Entity
    {
        [Required]
        public string Extension { get; set; }
        [Required]
        public Uri Location { get; set; }
        [Required]
        public string SystemFileName { get; set; }
        [Required]
        public string OriginalName { get; set; }
        public long SizeInBytes { get; set; }

    }
}