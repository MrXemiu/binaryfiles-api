﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BinaryFiles.Domain.Entities
{
    public class User : Entity
    {
        [Required]
        public string UserName { get; set; }

        public virtual List<UserFileAccess> UserFiles { get; set; } = new List<UserFileAccess>();

        public User()
        {
        }
    }
}
