﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BinaryFiles.Domain.Entities
{
    public class UserFileAccess : Entity
    {
        [Required]
        public FileAccessType AccessType { get; set; }

        public int FileId { get; set; }
        public virtual File File { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
