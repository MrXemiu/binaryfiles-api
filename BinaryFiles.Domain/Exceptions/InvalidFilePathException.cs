﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryFiles.Domain.Exceptions
{
    public class InvalidFilePathException : Exception
    {
        public InvalidFilePathException()
        {

        }

        public InvalidFilePathException(string message):base(message)
        {
            
        }

        public InvalidFilePathException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
