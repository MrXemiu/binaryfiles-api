﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryFiles.Domain.DataAccess;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.Services
{
    public class FileAccessService : IFileAccessService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FileAccessService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<UserFileAccess>> GetUserFiles(int userId)
        {
            var userFileRepository = _unitOfWork.GetRepository<UserFileAccess>();
            var userFiles = await userFileRepository.ListAsync(access => access.UserId == userId);
            return userFiles.ToList();
        }

        public async Task<UserFileAccess> GetUserFile(int userId, int fileId)
        {
            var userFileRepository = _unitOfWork.GetRepository<UserFileAccess>();
            var userFiles = await userFileRepository.ListAsync(access => access.UserId == userId && access.FileId == fileId);
            //TODO ensure that userfileaccess is unique for a given userid/fileid combination
            return userFiles.SingleOrDefault();
        }

        public async Task<int> AddUserFile(int userId, int fileId, FileAccessType fileAccessType)
        {
            var newUserFile = new UserFileAccess
            {
                UserId = userId,
                FileId = fileId,
                AccessType = fileAccessType
            };
            var userFileRepository = _unitOfWork.GetRepository<UserFileAccess>();
            await userFileRepository.AddAsync(newUserFile);
            await _unitOfWork.CommitAsync();
            return newUserFile.Id;
        }
    }
}