﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BinaryFiles.Domain.DataAccess;
using BinaryFiles.Domain.Entities;
using File = BinaryFiles.Domain.Entities.File;

namespace BinaryFiles.Domain.Services
{
    public class FileService : IFileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBinaryService _binaryService;
        private readonly IContextProvider _contextProvider;
        private readonly IUserService _userService;
        private readonly IFileAccessService _fileAccessService;

        public FileService(
            IUnitOfWork unitOfWork, 
            IBinaryService binaryService, 
            IContextProvider contextProvider,
            IUserService userService, 
            IFileAccessService fileAccessService)
        {
            _unitOfWork = unitOfWork;
            _binaryService = binaryService;
            _contextProvider = contextProvider;
            _userService = userService;
            _fileAccessService = fileAccessService;
        }

        /// <summary>
        /// Given a valid user ID, returns a collection of File records, but does not return actual binary data.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<File>> GetUserFiles(int userId)
        {
            //TODO ensure that related entities come along 
            var userFiles = await _fileAccessService.GetUserFiles(userId);
            return userFiles.Select(access => access.File);
        }

        public async Task<(File, Stream)> GetFileStream(int fileId)
        {
            var fileRepository = _unitOfWork.GetRepository<File>();
            var fileRecord = await fileRepository.GetByIdAsync(fileId);
            var fileUri = fileRecord.Location;
            var fileStream = _binaryService.GetBinaryFileStream(fileUri);
            return (fileRecord, fileStream);
        }

        public Task<(File, Stream)> GetFileStream(string originalFileName)
        {
            //TODO implement file search by originalFileName
            throw new NotImplementedException();
        }

        public async Task<int> AddFile(string fileName, Stream contentStream)
        {
            Uri fileUri = null;

            try
            {
                //get userId
                var ownerName = await _contextProvider.GetCurrentUserName();
                var ownerUser = await _userService.GetUser(ownerName);
                if (ownerUser == null) throw new Exception($"Invalid user: {ownerName}.");

                //TODO check for existing file
                //TODO decide how to handle existing file, eg throw exception or replace file if user access == write

                //store file
                //NOTE  consider refactoring into separate method
                var systemFileName = GetRandomFileName(fileName);
                fileUri = await _binaryService.SaveBinaryFileStream(systemFileName, contentStream, null);

                //create file record
                //NOTE  consider refactoring into separate method
                var fileEntity = new File
                {
                    Location = fileUri,
                    OriginalName = fileName,
                    SystemFileName = systemFileName,
                    IsDeleted = false,
                    Extension = Path.GetExtension(fileName),
                    SizeInBytes = contentStream.Length
                };
                //await _unitOfWork.GetRepository<File>().AddAsync(fileEntity);

                //create file access record
                //NOTE  consider refactoring into separate method
                var fileAccessRecord = new UserFileAccess
                {
                    UserId = ownerUser.Id,
                    AccessType = FileAccessType.ReadWrite,
                    File = fileEntity,
                    User = ownerUser
                };
                await _unitOfWork.GetRepository<UserFileAccess>().AddAsync(fileAccessRecord);

                //commit transaction
                await _unitOfWork.CommitAsync(true);
                return fileAccessRecord.FileId;
            }
            catch (Exception)
            {
                //delete file
                if (fileUri != null)
                    _binaryService.Delete(fileUri);

                throw;
            }
        }

        public async Task DeleteFile(int fileId)
        {
            var (fileRecord, fileStream) = await GetFileStream(fileId);
            try
            {
                var fileRepository = _unitOfWork.GetRepository<File>();
                fileRepository.Delete(fileRecord);

                fileStream.Close();
                _binaryService.Delete(fileRecord.Location);

                await _unitOfWork.CommitAsync();
            }
            catch (Exception)
            {
                //TODO figure out how get the optional subdirectory so we can restore it
                //TODO figure out how to restore the file
                throw;
            }
        }

        /// <summary>
        /// For security reasons file should be given new names for storage within our system.
        /// </summary>
        /// <param name="originalFileName"></param>
        /// <returns>new random name with original file extension</returns>
        public string GetRandomFileName(string originalFileName)
        {
            return $"{Path.GetRandomFileName()}{Path.GetExtension(originalFileName)}";
        }
    }
}