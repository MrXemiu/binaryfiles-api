﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace BinaryFiles.Domain.Services
{
    /// <summary>
    /// Implementers take and return Streams to allow for the possibility of different kinds of file storage provider implementations.
    /// Examples: Azure blob, Http, and local disk.
    /// </summary>
    public interface IBinaryService
    {
        Stream GetBinaryFileStream(Uri fileLocation);
        Task<Uri> SaveBinaryFileStream(string fileName, Stream inputStream, string subDirectory = null);
        void Delete(Uri filePath);
    }
}
