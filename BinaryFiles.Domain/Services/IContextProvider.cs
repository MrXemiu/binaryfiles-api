﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BinaryFiles.Domain.Services
{
    public interface IContextProvider
    {
        Task<string> GetCurrentUserName();
    }
}
