﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.Services
{
    public interface IFileAccessService
    {
        Task<List<UserFileAccess>> GetUserFiles(int userId);
        Task<UserFileAccess> GetUserFile(int userId, int fileId);
        Task<int> AddUserFile(int userId, int fileId, FileAccessType fileAccessType);

    }
}
