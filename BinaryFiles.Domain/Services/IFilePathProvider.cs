﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BinaryFiles.Domain.Services
{
    /// <summary>
    /// Interface used to generate file paths for file storage and retrieval.
    /// </summary>
    public interface IFilePathProvider
    {
        Task<Uri> GetPath(string fileName, string subDirectory = null);
    }
}
