﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BinaryFiles.Domain.Entities;
using File = BinaryFiles.Domain.Entities.File;

namespace BinaryFiles.Domain.Services
{
    public interface IFileService
    {
        Task<IEnumerable<File>> GetUserFiles(int userId);
        Task<(File, Stream)> GetFileStream(int fileId);
        Task<(File, Stream)> GetFileStream(string originalFileName);
        Task<int> AddFile(string fileName, Stream contentStream);
        Task DeleteFile(int fileId);
        string GetRandomFileName(string originalFileName);
    }
}