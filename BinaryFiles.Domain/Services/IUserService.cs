﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.Services
{
    public interface IUserService
    {
        Task<User> GetUser(string userName);
        Task<User> GetUser(int userId);
        Task<int> AddUser(User user);
        Task DeleteUser(int userId);
    }
}
