﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryFiles.Domain.DataAccess;
using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<User> GetUser(string userName)
        {
            var userRepository = _unitOfWork.GetRepository<User>();
            var userRecord =
                await userRepository.ListAsync(user => user.UserName == userName);
            return userRecord.SingleOrDefault();
        }

        public async Task<User> GetUser(int userId)
        {
            var userRepository = _unitOfWork.GetRepository<User>();
            return await userRepository.GetByIdAsync(userId);
        }

        public async Task<int> AddUser(User user)
        {
            var userRepository = _unitOfWork.GetRepository<User>();
            await userRepository.AddAsync(user);
            await _unitOfWork.CommitAsync();
            return user.Id;
        }

        public async Task DeleteUser(int userId)
        {
            var userRepository = _unitOfWork.GetRepository<User>();
            var user = await GetUser(userId);
            userRepository.Delete(user);
            await _unitOfWork.CommitAsync();
        }
    }
}
