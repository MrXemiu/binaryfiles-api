﻿using System;
using BinaryFiles.Domain;
using BinaryFiles.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BinaryFiles.EntityFramework
{
    public class ApplicationContext : DbContext
    {
        public DbSet<File> Files { get; set; }

        public DbSet<UserFileAccess> UserFiles { get; set; }

        public DbSet<User> Users { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureUserEntity(modelBuilder.Entity<User>());
            ConfigureFileEntity(modelBuilder.Entity<File>());
            ConfigureUserFileAccessEntity(modelBuilder.Entity<UserFileAccess>());
        }

        private static void ConfigureUserEntity(EntityTypeBuilder<User> userEntity)
        {
            userEntity.HasIndex(user => user.UserName).IsUnique();
            userEntity.Property(user => user.UserName).HasColumnType("TEXT COLLATE NOCASE");

            userEntity.Property(file => file.IsDeleted)
                .HasDefaultValue(false);

            var anonymousUser = new User
            {
                Id = 1
                , UserName = Constants.DefaultUserName
            };
            userEntity.HasData(anonymousUser);
        }

        private static void ConfigureFileEntity(EntityTypeBuilder<File> fileEntity)
        {
            fileEntity
                .Property(file => file.IsDeleted)
                .HasDefaultValue(false);

            fileEntity.HasIndex(file => file.OriginalName);

            fileEntity.HasIndex(file => file.Location)
                .IsUnique();
        }

        private static void ConfigureUserFileAccessEntity(EntityTypeBuilder<UserFileAccess> fileAccessEntity)
        {
            fileAccessEntity.Property(file => file.IsDeleted)
                .HasDefaultValue(false);

            fileAccessEntity
                .Property(access => access.AccessType)
                .HasConversion<string>();
        }
    }
}