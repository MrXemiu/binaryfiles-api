﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BinaryFiles.Domain.DataAccess;
using BinaryFiles.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BinaryFiles.EntityFramework.DataAccess
{
    public class UnitOfWork: IUnitOfWork, IRepositoryFactory
    {
        private readonly ApplicationContext _dbContext;

        private Dictionary<(Type type, string name), object> _repositories;

        public UnitOfWork(ApplicationContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dbContext.Database.EnsureCreated();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return (IRepository<TEntity>)GetOrAddRepository(typeof(TEntity), new Repository<TEntity>(_dbContext));
        }

        public int Commit(bool autoHistory = false)
        {
            if (autoHistory) _dbContext.EnsureAutoHistory();
            return _dbContext.SaveChanges();
        }

        public async Task<int> CommitAsync(bool autoHistory = false)
        {
            if (autoHistory) _dbContext.EnsureAutoHistory();

            return await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }

        internal object GetOrAddRepository(Type type, object repo)
        {
            _repositories ??= new Dictionary<(Type type, string Name), object>();

            if (_repositories.TryGetValue((type, repo.GetType().FullName), out var repository)) return repository;
            _repositories.Add((type, repo.GetType().FullName), repo);
            return repo;
        }
    }
}
