﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using BinaryFiles.EntityFramework.DataAccess;

namespace BinaryFiles.EntityFramework
{
    public class EntityFrameworkModule: Module
    {
        #region Overrides of Module


        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(EntityFrameworkModule).Assembly;

            builder.RegisterType<UnitOfWork>()
                .InstancePerLifetimeScope()
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.Name.EndsWith("Provider")
                               || type.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
        }


        #endregion
    }
}
