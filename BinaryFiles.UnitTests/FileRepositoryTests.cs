﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BinaryFiles.Domain.Tests.Infrastructure;
using BinaryFiles.Domain.Tests.TestData;
using BinaryFiles.EntityFramework;
using BinaryFiles.EntityFramework.DataAccess;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;
using File = BinaryFiles.Domain.Entities.File;

namespace BinaryFiles.Domain.Tests
{
    public abstract class FileRepositoryTests : DomainLayerTests
    {
        protected FileRepositoryTests(DbContextOptions<ApplicationContext> contextOptions) : base(contextOptions)
        {
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetById_GivenValidId_ReturnsCorrectFileEntity(int id)
        {
            //arrange
            var seedHelper = new SeedHelper();
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();
            //ids will start at 1 and increment; array index starts at 0, so the corresponding file in the helper class is at position id-1
            var testFile = seedHelper.Files[id - 1];

            //act
            var fileRecord = fileRepo.GetById(id);

            //assert
            fileRecord.Should()
                .NotBeNull();
            fileRecord.Id.Should()
                .Be(id);
            fileRecord.Location.Should()
                .Be(testFile.Location);
            fileRecord.OriginalName.Should()
                .Be(testFile.OriginalName);
            fileRecord.SizeInBytes.Should()
                .Be(testFile.SizeInBytes);
            fileRecord.SystemFileName.Should()
                .Be(testFile.SystemFileName);

            foreach (var entry in context.ChangeTracker.Entries())
                entry.State = EntityState.Detached;
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetByIdAsync_GivenValidId_ReturnsCorrectFileEntity(int id)
        {
            //arrange
            var seedHelper = new SeedHelper();
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();
            //ids will start at 1 and increment; array index starts at 0, so the corresponding file in the helper class is at position id-1
            var testFile = seedHelper.Files[id - 1];

            //act
            var fileRecord = await fileRepo.GetByIdAsync(id);

            //assert
            fileRecord.Should()
                .NotBeNull();
            fileRecord.Id.Should()
                .Be(id);
            fileRecord.Location.Should()
                .Be(testFile.Location);
            fileRecord.OriginalName.Should()
                .Be(testFile.OriginalName);
            fileRecord.SizeInBytes.Should()
                .Be(testFile.SizeInBytes);
            fileRecord.SystemFileName.Should()
                .Be(testFile.SystemFileName);

            foreach (var entry in context.ChangeTracker.Entries())
                entry.State = EntityState.Detached;
        }

        private File MakeNewFileEntity(string testFileName)
        {
            return new File
            {
                Location = new Uri(Path.Combine(Path.GetTempPath(), testFileName))
                , Extension = Path.GetExtension(testFileName)
                , OriginalName = testFileName
                , SizeInBytes = 1024
                , SystemFileName = testFileName
            };
        }


        [Fact]
        public void Add_InsertsFileEntityRecord()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();


            //act
            var newFile = MakeNewFileEntity($"{nameof(Add_InsertsFileEntityRecord)}.test");
            fileRepo.Add(newFile);
            uow.Commit();

            //assert
            newFile.Id.Should()
                .BeGreaterThan(0);
        }

        [Fact]
        public async Task AddAsync_InsertsFileEntityRecord()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();


            //act
            var newFile = MakeNewFileEntity($"{nameof(AddAsync_InsertsFileEntityRecord)}.test");
            await fileRepo.AddAsync(newFile);
            var rowCount = await uow.CommitAsync();

            //assert
            rowCount.Should()
                .Be(1);
            newFile.Id.Should()
                .BeGreaterThan(0);
        }

        [Fact]
        public void Delete_HardDeletesFileEntity()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();
            var fileToDelete = context.Set<File>()
                .First();

            //act
            fileRepo.Delete(fileToDelete);
            var rowCount = uow.Commit();

            //assert
            rowCount.Should()
                .Be(1);
            context.Entry(fileToDelete)
                .State.Should()
                .Be(EntityState.Detached);
            context.Files.Any(file => file.Id == fileToDelete.Id)
                .Should()
                .BeFalse();
            context.UserFiles.Any(access => access.FileId == fileToDelete.Id)
                .Should()
                .BeFalse();
        }

        [Fact]
        public void Delete_SoftDeletesFileEntity()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();
            var fileToDelete = context.Set<File>()
                .First();

            //act
            fileRepo.Delete(fileToDelete);
            var rowCount = uow.Commit();

            //assert
            rowCount.Should()
                .Be(1);
            fileToDelete.IsDeleted.Should()
                .BeTrue();
        }

        [Fact]
        public void GetById_GivenInvalidId_ReturnsNull()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();

            //act
            var fileRecord = fileRepo.GetById(100);

            //assert
            fileRecord.Should()
                .BeNull();
        }

        [Fact]
        public async Task GetByIdAsync_GivenInvalidId_ReturnsNull()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();

            //act
            var fileRecord = await fileRepo.GetByIdAsync(100);

            //assert
            fileRecord.Should()
                .BeNull();
        }

        [Fact]
        public void List_ReturnsListOfFileEntities()
        {
            //arrange
            var seedHelper = new SeedHelper();
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();

            //act
            var fileRecords = fileRepo.List();

            //assert
            fileRecords.Count()
                .Should()
                .BeGreaterOrEqualTo(seedHelper.Files.Length);
        }

        [Fact]
        public async Task ListAsync_ReturnsListOfFileEntities()
        {
            //arrange
            var seedHelper = new SeedHelper();
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();

            //act
            var fileRecords = await fileRepo.ListAsync();

            //assert
            fileRecords.Count()
                .Should()
                .BeGreaterOrEqualTo(seedHelper.Files.Length);
        }

        [Fact]
        public void Update_UpdatesFileEntity()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);
            var fileRepo = uow.GetRepository<File>();
            var fileToUpdate = context.Set<File>()
                .First();
            var newFileName = $"{nameof(Update_UpdatesFileEntity)}{Path.GetExtension(fileToUpdate.OriginalName)}";
            fileToUpdate.OriginalName = newFileName;

            //act
            fileRepo.Update(fileToUpdate);
            var rowCount = uow.Commit();

            //assert
            rowCount.Should()
                .Be(1);
        }
    }
}