﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extras.Moq;
using BinaryFiles.API;
using BinaryFiles.Application;
using BinaryFiles.DiskStorage;
using BinaryFiles.Domain.Entities;
using BinaryFiles.Domain.Services;
using BinaryFiles.Domain.Tests.Infrastructure;
using BinaryFiles.Domain.Tests.TestData;
using BinaryFiles.EntityFramework;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;
using File = System.IO.File;

namespace BinaryFiles.Domain.Tests
{
    public abstract class FileServiceTests : DomainLayerTests
    {
        protected FileServiceTests(DbContextOptions<ApplicationContext> contextOptions) : base(contextOptions)
        {
            var currentDirectory = Environment.CurrentDirectory;
            _testDirectory = Path.Combine(currentDirectory, "tests");
            if (!Directory.Exists(_testDirectory))
                Directory.CreateDirectory(_testDirectory);
        }

        private readonly string _testDirectory;

        [Theory]
        [InlineData("test", "")]
        [InlineData("test.zip", ".zip")]
        [InlineData("foo\\test.exe", ".exe")]
        [InlineData("foo/test.pdf", ".pdf")]
        [InlineData("foo\\bar\\baz\\test.c", ".c")]
        public void GetRandomFileName_GivenFileNameWithExtension_ReturnsRandomFileNameWithSameExtension(string fileName, string extension)
        {
            //TODO figure out how to deal with files that don't have extensions

            //arrange
            using var mock = AutoMock.GetLoose();
            var sut = mock.Create<FileService>();

            //act
            var newFileName = sut.GetRandomFileName(fileName);

            //assert
            newFileName.Should()
                .NotBeEquivalentTo(fileName);

            Path.GetExtension(newFileName)
                .Should()
                .BeEquivalentTo(extension);
        }

        [Theory(Skip = "Not implemented")]
        [InlineData(1, 1)]
        public void GetFileStream_GivenValidFileId_ReturnsStreamWithCorrectLength(int fileId, long bytes)
        {
            var foo = fileId;
            var bar = bytes;
            //TODO figure out a smart way to write the integration test
        }

        [Theory]
        [InlineData("1mb.test")]
        [InlineData("50mb.test")]
        [InlineData("100mb.test")]
        public async Task AddFile_CreatesFileAndUserFileAccessRecords_AndWritesFileToDisk(string fileName)
        {
            //arrange
            var testFileName = $"./TestData/{fileName}";
            var options = new DiskStorageOptions
            {
                RootDirectory = _testDirectory
            };

            //setup DI container
            using var context = new ApplicationContext(ContextOptions);
            using var mock = AutoMock.GetLoose(builder =>
            {
                builder.RegisterInstance(context);
                builder.RegisterModule(new DomainModule());
                builder.RegisterModule(new EntityFrameworkModule());
                builder.RegisterModule(new DiskStorageModule());
                builder.RegisterModule(new ApplicationModule());
                builder.RegisterModule(new ApiModule());
            });
            mock.Mock<IOptions<DiskStorageOptions>>()
                .Setup(options1 => options1.Value)
                .Returns(options);

            //open a fileStream to a test file on disk, simulating a file upload
            using var fileStream = File.OpenRead(testFileName);

            //create an instance of the system under test (SUT)
            var fileService = mock.Create<FileService>();


            //act
            var fileId = await fileService.AddFile(fileName, fileStream);


            //assert
            fileId.Should()
                .BeGreaterThan(0);

            var (fileRecord, stream) = await fileService.GetFileStream(fileId);
            fileRecord.Extension.Should()
                .Be(Path.GetExtension(fileName));
            fileRecord.OriginalName.Should()
                .Be(fileName);
            stream.Length.Should()
                .Be(fileStream.Length);
        }

        [Fact]
        public async Task GetUserFiles_GivenUserWithFiles_ReturnsUsersFiles()
        {
            //arrange
            var userId = 1;
            var seedHelper = new SeedHelper();
            using var context = new ApplicationContext(ContextOptions);
            var userFilesTestData = new[]
            {
                new UserFileAccess
                {
                    UserId = userId
                    , AccessType = FileAccessType.ReadWrite
                    , File = seedHelper.Files[0]
                }
                , new UserFileAccess
                {
                    UserId = userId
                    , AccessType = FileAccessType.ReadWrite
                    , File = seedHelper.Files[1]
                }
                , new UserFileAccess
                {
                    UserId = userId
                    , AccessType = FileAccessType.ReadWrite
                    , File = seedHelper.Files[2]
                }
            };

            //setup DI container
            using var mock = AutoMock.GetLoose(builder => { builder.RegisterInstance(context); });
            mock.Mock<IFileAccessService>()
                .Setup(service => service.GetUserFiles(userId))
                .ReturnsAsync(userFilesTestData.ToList);
            var sut = mock.Create<FileService>();

            var files = await sut.GetUserFiles(userId);
            files.Should()
                .NotBeNullOrEmpty();
            files.Count()
                .Should()
                .Be(3);
            files.Should()
                .AllBeOfType<Domain.Entities.File>();
        }
    }
}