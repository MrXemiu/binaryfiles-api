﻿using System;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using BinaryFiles.DiskStorage;
using BinaryFiles.DiskStorage.Services;
using BinaryFiles.Domain.Exceptions;
using BinaryFiles.Domain.Services;
using BinaryFiles.Domain.Tests.TestData;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace BinaryFiles.Domain.Tests
{
    public class HardDiskFilePathProviderTests
    {
        [Theory]
        [InlineData(@"c:\", "42aae06e-017d-4fd7-925d-920391f1ed06")]
        [InlineData(@"c:\", "42aae06e-017d-4fd7-925d-920391f1ed06.dll")]
        [InlineData(@"c:\temp", "42aae06e-017d-4fd7-925d-920391f1ed06.dll", "images")]
        [InlineData(@"c:\temp", "42aae06e-017d-4fd7-925d-920391f1ed06.dll", "sql/scripts")]
        public async Task GetPath_ReturnsValidFileUri(string rootDirectory
            , string fileName
            , string subDirectory = null)
        {
            //arrange
            var options = new DiskStorageOptions
            {
                RootDirectory = rootDirectory
            };
            var user = EntityFactory.MakeUser(Constants.DefaultUserName);

            using var mock = AutoMock.GetLoose();
            mock.Mock<IOptions<DiskStorageOptions>>()
                .Setup(x => x.Value)
                .Returns(options);
            mock.Mock<IContextProvider>()
                .Setup(provider => provider.GetCurrentUserName())
                .ReturnsAsync(Constants.DefaultUserName);
            mock.Mock<IUserService>()
                .Setup(service => service.GetUser(Constants.DefaultUserName))
                .ReturnsAsync(user);
            var sut = mock.Create<HardDiskFilePathProvider>();

            //act
            var fileUri = await sut.GetPath(fileName, subDirectory);

            //assert
            fileUri.AbsolutePath.Should()
                .NotBeNullOrWhiteSpace();
            fileUri.IsFile.Should()
                .BeTrue();
            fileUri.Segments.Should()
                .Contain(s => s.Contains($"{user.Id}/"));
            fileUri.Segments.Should()
                .Contain(fileName);
        }

        [Theory]
        [InlineData(@"", "42aae06e-017d-4fd7-925d-920391f1ed06.dll")]
        [InlineData(@"temp", "42aae06e-017d-4fd7-925d-920391f1ed06.dll", "images")]
        [InlineData(@"\temp", "42aae06e-017d-4fd7-925d-920391f1ed06.dll", "sql/scripts")]
        [InlineData(@"..\..\temp", "42aae06e-017d-4fd7-925d-920391f1ed06.dll", "sql/scripts")]
        public void GetPath_ThrowsInValidPathException(string rootDirectory
            , string fileName
            , string subDirectory = null)
        {
            //arrange
            var options = new DiskStorageOptions
            {
                RootDirectory = rootDirectory
            };
            var user = EntityFactory.MakeUser(Constants.DefaultUserName);

            using var mock = AutoMock.GetLoose();
            mock.Mock<IOptions<DiskStorageOptions>>()
                .Setup(x => x.Value)
                .Returns(options);
            mock.Mock<IContextProvider>()
                .Setup(provider => provider.GetCurrentUserName())
                .ReturnsAsync(Constants.DefaultUserName);
            mock.Mock<IUserService>()
                .Setup(service => service.GetUser(Constants.DefaultUserName))
                .ReturnsAsync(user);
            var sut = mock.Create<HardDiskFilePathProvider>();

            //act
            Func<Task> act = async () => await sut.GetPath(fileName, subDirectory);

            //assert
            act.Should()
                .Throw<InvalidFilePathException>();
        }
    }
}