﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using BinaryFiles.DiskStorage.Services;
using BinaryFiles.Domain.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace BinaryFiles.Domain.Tests
{
    public class HardDiskFileServiceTests
    {
        public HardDiskFileServiceTests()
        {
            var currentDirectory = Environment.CurrentDirectory;
            _testDirectory = Path.Combine(currentDirectory, "tests");
            if (!Directory.Exists(_testDirectory))
                Directory.CreateDirectory(_testDirectory);
        }

        private readonly string _testDirectory;
        private const string _loremIpsumContent = "Lorem ipsum dolor sit amet.";
        private const string _loremIpsumTitle = "Lorem Ipsum.txt";

        [Theory]
        [InlineData("1mb.test")]
        [InlineData("50mb.test")]
        [InlineData("100mb.test")]
        public async Task SaveBinaryFileStream_GivenValidFileStream_ReturnsUriToFile(string fileName)
        {
            //arrange
            var targetDestinationUri = new Uri(Path.Combine(_testDirectory, fileName));

            using var mock = AutoMock.GetLoose();
            mock.Mock<IFilePathProvider>()
                .Setup(provider => provider.GetPath(fileName, null))
                .ReturnsAsync(targetDestinationUri);
            var sut = mock.Create<HardDiskFileService>();

            var testFileName = $"./TestData/{fileName}";
            using var fileStream = File.OpenRead(testFileName);

            //act
            var savedDestinationUri = await sut.SaveBinaryFileStream(fileName, fileStream);

            //assert
            savedDestinationUri.Should()
                .BeEquivalentTo(targetDestinationUri);
            File.Exists(savedDestinationUri.LocalPath)
                .Should()
                .BeTrue();
        }


        [Theory]
        [InlineData("1mb.test", 1048576)]
        [InlineData("50mb.test", 52428800)]
        [InlineData("100mb.test", 104857600)]
        public void GetBinaryFileStream_GivenValidFileStream_ReturnsUriToFile(string fileName, long bytes)
        {
            //arrange
            var filePath = Path.Combine(Environment.CurrentDirectory, "TestData", fileName);
            using var mock = AutoMock.GetLoose();
            var sut = mock.Create<HardDiskFileService>();

            //act
            using var fs = sut.GetBinaryFileStream(new Uri(filePath));

            //assert
            fs.CanRead.Should()
                .BeTrue();
            fs.Length.Should()
                .Be(bytes);
        }


        private byte[] GetBytes(string content)
        {
            return Encoding.ASCII.GetBytes(content);
        }

        private void WriteFile(string path, string fileName, byte[] bytes)
        {
            using var fileStream = File.OpenWrite(Path.Combine(path, fileName));
            using var binaryWriter = new BinaryWriter(fileStream);
            binaryWriter.Write(bytes);
        }

        [Fact]
        public void Delete_Works()
        {
            //arrange
            WriteFile(_testDirectory, _loremIpsumTitle, GetBytes(_loremIpsumContent));
            var uri = new Uri(Path.Combine(_testDirectory, _loremIpsumTitle));
            File.Exists(uri.LocalPath)
                .Should()
                .BeTrue();

            using var mock = AutoMock.GetLoose();
            var sut = mock.Create<HardDiskFileService>();

            //act
            sut.Delete(uri);

            //assert
            File.Exists(uri.LocalPath)
                .Should()
                .BeFalse();
        }
    }
}