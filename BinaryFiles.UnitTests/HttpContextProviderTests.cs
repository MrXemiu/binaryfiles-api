﻿using System.Threading.Tasks;
using Autofac.Extras.Moq;
using BinaryFiles.API.Services;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace BinaryFiles.Domain.Tests
{
    public class HttpContextProviderTests
    {
        [Fact]
        public async Task GetCurrentUserName_GivenDefaultHttpContext_ReturnsDefaultUserName()
        {
            //arrange
            using var mock = AutoMock.GetLoose();
            var context = new DefaultHttpContext();
            mock.Mock<IHttpContextAccessor>()
                .Setup(accessor => accessor.HttpContext)
                .Returns(context);
            var sut = mock.Create<HttpContextProvider>();

            //act 
            var userName = await sut.GetCurrentUserName();

            userName.Should()
                .BeEquivalentTo(Constants.DefaultUserName);
        }
    }
}