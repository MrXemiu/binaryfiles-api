﻿using System.Data.Common;
using BinaryFiles.EntityFramework;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BinaryFiles.Domain.Tests
{
    public class InMemoryUnitOfWorkTests : UnitOfWorkTests
    {
        private readonly DbConnection _connection;

        public InMemoryUnitOfWorkTests() : base(new DbContextOptionsBuilder<ApplicationContext>()
            .UseLazyLoadingProxies()
            .UseSqlite(CreateInMemoryDatabase(),
                builder => { builder.MigrationsAssembly(typeof(ApplicationContext).Assembly.ToString()); })
            .EnableSensitiveDataLogging()
            .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions)
                .Connection;
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }
    }
}