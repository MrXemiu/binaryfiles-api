﻿using BinaryFiles.Domain.Entities;
using BinaryFiles.Domain.Tests.TestData;
using BinaryFiles.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace BinaryFiles.Domain.Tests.Infrastructure
{
    public abstract class DomainLayerTests
    {
        protected DbContextOptions<ApplicationContext> ContextOptions { get; }

        protected DomainLayerTests(DbContextOptions<ApplicationContext> contextOptions)
        {
            ContextOptions = contextOptions;
            Seed();
        }

        private void Seed()
        {
            var seedHelper = new SeedHelper();

            using var context = new ApplicationContext(ContextOptions);

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var user0 = seedHelper.Users[0];
            user0.UserFiles.Add(
                new UserFileAccess
                {
                    File = seedHelper.Files[0]
                    , AccessType = FileAccessType.ReadWrite
                });
            context.Users.Add(user0);

            var user1 = seedHelper.Users[1];
            user1.UserFiles.Add(
                new UserFileAccess
                {
                    File = seedHelper.Files[1]
                    , AccessType = FileAccessType.ReadWrite
                });
            context.Users.Add(user1);

            var user2 = seedHelper.Users[2];
            user2.UserFiles.Add(
                new UserFileAccess
                {
                    File = seedHelper.Files[2]
                    , AccessType = FileAccessType.ReadWrite
                });
            context.Users.Add(user2);

            context.SaveChanges();
        }
    }
}