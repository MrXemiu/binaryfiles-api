﻿using System;
using System.IO;
using BinaryFiles.Domain.Entities;
using File = BinaryFiles.Domain.Entities.File;

namespace BinaryFiles.Domain.Tests.TestData
{
    public class SeedHelper
    {
        public File[] Files =
        {
            new File
            {
                Extension = "test"
                , Location = new Uri(Path.Combine(Path.GetTempPath(), "test1.test"))
                , OriginalName = "test1.test"
                , SizeInBytes = 1024
                , SystemFileName = "test1.test"
            }
            , new File
            {
                Extension = "test"
                , Location = new Uri(Path.Combine(Path.GetTempPath(), "test2.test"))
                , OriginalName = "test2.test"
                , SizeInBytes = 1024
                , SystemFileName = "test2.test"
            }
            , new File
            {
                Extension = "test"
                , Location = new Uri(Path.Combine(Path.GetTempPath(), "test3.test"))
                , OriginalName = "test3.test"
                , SizeInBytes = 1024
                , SystemFileName = "test3.test"
            }
        };

        public User[] Users =
        {
            new User
            {
                UserName = "User1"
            }
            , new User
            {
                UserName = "User2"
            }
            , new User
            {
                UserName = "User3"
            }
        };
    }
}