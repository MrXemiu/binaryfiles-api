﻿using BinaryFiles.Domain.Entities;

namespace BinaryFiles.Domain.Tests.TestData
{
    public static class EntityFactory
    {
        private static int _userIdSequence;

        public static User MakeUser(string userName)
        {
            var user = new User
            {
                Id = _userIdSequence
                , UserName = userName
            };
            _userIdSequence++;
            return user;
        }
    }
}