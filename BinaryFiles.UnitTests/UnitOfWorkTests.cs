﻿using BinaryFiles.Domain.Entities;
using BinaryFiles.Domain.Tests.Infrastructure;
using BinaryFiles.EntityFramework;
using BinaryFiles.EntityFramework.DataAccess;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BinaryFiles.Domain.Tests
{
    public abstract class UnitOfWorkTests : DomainLayerTests
    {
        protected UnitOfWorkTests(DbContextOptions<ApplicationContext> contextOptions) : base(contextOptions)
        {
        }

        [Fact]
        public void GetRepository_File_ReturnsFileRepository()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);

            //act
            var fileRepo = uow.GetRepository<File>();

            //assert
            fileRepo.GetType()
                .Should()
                .Be<Repository<File>>();
            fileRepo.Should()
                .NotBeNull();
        }

        [Fact]
        public void GetRepository_User_ReturnsUserRepository()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);

            //act
            var userRepo = uow.GetRepository<User>();

            //assert
            userRepo.GetType()
                .Should()
                .Be<Repository<User>>();
            userRepo.Should()
                .NotBeNull();
        }

        [Fact]
        public void GetRepository_UserFileAccess_ReturnsUserFileAccessRepository()
        {
            //arrange
            using var context = new ApplicationContext(ContextOptions);
            using var uow = new UnitOfWork(context);

            //act
            var userfileAccessRepo = uow.GetRepository<UserFileAccess>();

            //assert
            userfileAccessRepo.GetType()
                .Should()
                .Be<Repository<UserFileAccess>>();
            userfileAccessRepo.Should()
                .NotBeNull();
        }
    }
}