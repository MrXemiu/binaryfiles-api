﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using BinaryFiles.API.Services;

namespace BinaryFiles.API
{
    public class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ApiModule).Assembly;
            
            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.Name.EndsWith("Provider")
                               || type.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
        }
    }
}
