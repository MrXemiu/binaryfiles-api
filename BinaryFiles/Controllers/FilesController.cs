﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryFiles.Application.Dtos;
using BinaryFiles.Application.Services;
using BinaryFiles.Domain.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BinaryFiles.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]"), AllowAnonymous]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IFileApplicationService _fileApplicationService;
        private readonly IContextProvider _contextProvider;
        private readonly IUserService _userService;

        public FilesController(IFileApplicationService fileApplicationService, IContextProvider contextProvider, IUserService userService)
        {
            _fileApplicationService = fileApplicationService;
            _contextProvider = contextProvider;
            _userService = userService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(FileStreamResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var fileDto = await _fileApplicationService.Get(id);
                
                if (fileDto == null)  return NotFound();
                
                return new FileStreamResult(fileDto.DataStream, fileDto.MimeType);
            }
            catch (Exception e)
            {
                throw new Exception("Failed to process get stream request.", e);
            }
        }

        [HttpGet("all")]
        [ProducesResponseType(typeof(ActionResult<IEnumerable<FileDto>>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<FileDto>>> GetAll()
        {
            try
            {
                var currentUserName = await _contextProvider.GetCurrentUserName();
                if (string.IsNullOrWhiteSpace(currentUserName))  throw new NullReferenceException(nameof(currentUserName));

                //TODO get clever and figure out the issues with lazy/eager loading and the user entity, since it has a UserFileAccess collection navigation property
                var currentUser = await _userService.GetUser(currentUserName);
                if(currentUser == null) throw new NullReferenceException(nameof(currentUser));

                var fileDtos = await _fileApplicationService.GetAllForUser(currentUser.Id);
                return new ActionResult<IEnumerable<FileDto>>(fileDtos);
            }
            catch (Exception e)
            {
                throw new Exception("Failed to process list files request.", e);
            }
        }

        [HttpPost("upload")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<IActionResult> Upload(IFormFile formFile)
        {
            try
            {
                if (formFile == null) return BadRequest();

                await _fileApplicationService.Save(formFile);
                //TODO update this to use Created() result
                return Accepted();
            }
            catch (Exception e)
            {
                throw new Exception("Failed to process file upload request.", e);
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var success = await _fileApplicationService.Delete(id);
                return success ? Ok() : new StatusCodeResult(StatusCodes.Status304NotModified);
            }
            catch (Exception e)
            {
                throw new Exception("Failed to process delete file request.", e);
            }
        }

    }
}
