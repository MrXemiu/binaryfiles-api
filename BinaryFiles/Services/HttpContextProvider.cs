﻿using System.Threading.Tasks;
using BinaryFiles.Domain;
using BinaryFiles.Domain.Services;
using Microsoft.AspNetCore.Http;

namespace BinaryFiles.API.Services
{
    public class HttpContextProvider : IContextProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextProvider(IHttpContextAccessor httpContextAccessor )
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<string> GetCurrentUserName()
        {
            var currentUserName = _httpContextAccessor?.HttpContext?.User?.Identity?.Name ?? Constants.DefaultUserName;
            return await Task.FromResult(currentUserName);
        }
    }
}