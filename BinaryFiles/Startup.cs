using System;
using Autofac;
using BinaryFiles.API;
using BinaryFiles.API.Services;
using BinaryFiles.Application;
using BinaryFiles.DiskStorage;
using BinaryFiles.Domain;
using BinaryFiles.EntityFramework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BinaryFilleWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public ILifetimeScope AutofacContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHttpContextAccessor();
            services.AddDbContext<ApplicationContext>(options =>
            {
                // this tight coupling to in-memory sqlite is a convenience for portability
                // TODO: refactor into something configurable/flexible.  
                options.UseLazyLoadingProxies()
                    .UseSqlite(@"Data Source=binaryFiles.db",
                        builder => { builder.MigrationsAssembly(typeof(ApplicationContext).Assembly.FullName); });
            });

            //configure and validate the disk storage-specific options in the DiskStorage section of appsettings.json
            //quick and dirty way of trying to ensure that an absolute path, (instead of a relative or string/whitespace), is defined in the config
            services.AddOptions<DiskStorageOptions>()
                .Bind(Configuration.GetSection(DiskStorageOptions.DiskStorage))
                .Validate(options => Uri.TryCreate(options.RootDirectory, UriKind.Absolute, out var rootDirectory));

            services.AddSwaggerGen();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "BinaryFiles API V1"); });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new DomainModule());
            builder.RegisterModule(new EntityFrameworkModule());
            builder.RegisterModule(new DiskStorageModule());
            builder.RegisterModule(new ApplicationModule());
            builder.RegisterModule(new ApiModule());
        }
    }
}