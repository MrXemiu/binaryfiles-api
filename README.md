﻿# BinaryFiles API Documentation

## Project Guidelines

![senior software engineer code sample](senrior%20software%20engineer%20project.png)

## Overview

The BinaryFiles API is a web service for managing binary files.  The API provides clients with end points to upload, download, list, and delete file data.

The API currently provides a single upload method that processes a single binary file at a time submitted via form POST.  The drawback with this method is that system resources could be exhausted more quickly than a streaming solution, but given time constraints and ambitions, I opted to go for the simpler to implement, and simpler to test implementation.

This initial implementation treats all users as anonymous, and therefore grants all users the same access to upload, download, list, and delete files.  However, the architecture can support multi-user scenarios in which one or more users have varying levels of access to any given file.

The application makes use of a database to store metadata about files under management, decoupling the ability to locate file data from its physical storage.  Additionally, file I/O operations may be more expensive and less optimizable compared to a database and its query engine, which could be important in the future.  

Special attention was taken to make some of the subsytems modular to allow for the possibility of better/different implementations.  

The WebAPI application is very shallow, acting mostly as a facade for the Application layer.  This should make it simple to adapt this application's library components for use with alternative web service implementations such as Azure Functions, or even Service Fabric if high performance is required and I'm feeling masochistic.  

The DiskStorage assembly is not much more than a hard drive-based implementation of the domain layer interface `IBinaryService`:  

```java  
    /// <summary>
    /// Implementers take and return Streams to allow for the possibility of different kinds of file storage provider implementations.
    /// Examples: Azure blob, Http, and local disk.
    /// </summary>
    public interface IBinaryService
    {
        Stream GetBinaryFileStream(Uri fileLocation);
        Task<Uri> SaveBinaryFileStream(string fileName, Stream inputStream, string subDirectory = null);
        void Delete(Uri filePath);
    }
```  

In some ways this is the heart of the application, and providing implementations for alternative file stores, such as to Azure Blob or a database can be done withing changing existing code.

## User stories

* As a **user** I want to upload files so that I can access (download) them later.
* As a **user** I want to list all my files so that I can know what I've previously uploaded.
* As a **user** I want to access (download) specific files I've previously uploaded so that I can use them.
* As a **user** I want to delete specific files I've previously uploaded so that they're no longer accessible.

#### For future consideration

* As a **user** I want access to my files to be restricted so that unauthorized users cannot access them.  
* As a **user** I want to list a subset of my files according to given criteria so that I can know which files match the criteria.  (this is a nice-to-have; at this point the list method lists everything)
* As a **user** I want to be able to undelete files I've previously deleted so that I can recover from mistakes.  (soft delete)  
* As a **user** I want to receive error and information messages in my prefered language so that I understand the messages.  (localize string constants)  
* As a **user** I would like to access an OData endpoint so that I filter the view of my files in order to make working with them more efficent.
* **System** must be able to synchronize files on disk with database so that files aren't orphaned if their corresponding database entries suffer corruption.  

## Use Case Analysis

#### Actors
**User** : for the initial implementation of this application, there will be a single user proxied for all users accessing the application since authorization is not required at this time.  The user represents any client agent accessing the BinaryFiles API application.  

**System** : server-side actor; executes business logic and issues HTTP responses to HTTP requests.  

#### Domain Entities
* __User__ - an entity to which file access is assigned.  
* __File__ - binary data.  
* __Audit Record__ - a record for a change to a record in the database.  

#### Use Cases
* The **user** sends an HTTP POST request to the **system** containing *valid* file data _TODO: specify how the data is sent_ and receives a response confirming success from the **system**.  
* The **user** sends an HTTP POST request to the **system** containing *invalid* file data _TODO: specify how the data is sent_ and receives a detailed failure message from the **system** in response.  
* The **user** sends an HTTP GET request to the **system** containing a *valid* file ID querystring argument and receives the corresponding file from the **system** in response.  
* The **user** sends an HTTP GET request to the **system** containing an *invalid* file ID querystring argument and receives a detailed failure message from the **system** in response.  
* The **user** sends an HTTP GET request to the **system** without any arguments and receives a list of files they're authorized to access from the **system** in response.  
* The **user** sends an HTTP DELETE request to the **system** containing an *valid* file ID querystring argument and receives a response confirming success from the **system**.  
* The **user** sends an HTTP DELETE request to the **system** containing an *invalid* file ID querystring argument and receives a detailed failure message from the **system** in response.  
* The **system** receives an HTTP GET request containing a *valid* file ID  querystring argument, queries the database for the corresponding record, retrieves the file from the location identified by the file record, and returns the file to the **user** in response.  
* The **system** receives an HTTP GET request containing an *invalid* file ID querystring argument and returns a detailed error message to the **user** in response.  
* The **system** receives an HTTP GET request without any querystring arguments and returns a list of files the **user** is authorized to access in response.  
* The **system** receives an HTTP POST request containing file data with a *unique* file name, then saves the file to disk, creates a corresponding file record in the database, creates an audit record for the transaction, and returns a success response to the **user**.  
* The **system** receives an HTTP POST request containing file data with an *existing* (non-unique) file name, ignores the file data, and returns a failure response to the **user**, informing them of the collision.  

## Architectural Considerations

The application architecture should be testable, extensible, and maintainable, and it should accomplish those goals using popular programming and architectural patterns to ease the inclusion of future contributors.  

Testability, extensibility, and maintainabilty are all well served by seperating categorically similar elements of the software into a layered architecture.  Within each layer, classes and data structures should be designed to have single, specific responsibilities, and depend on abstractions instead of concretions, ([S](https://en.wikipedia.org/wiki/Single-responsibility_principle) & [D](https://en.wikipedia.org/wiki/Dependency_inversion_principle) in [SOLID](https://en.wikipedia.org/wiki/SOLID)).  

To accomplish the aforementioned goals, the BinaryFiles API application makes use of concepts from [Domain Driven Design (DDD)](https://aspnetboilerplate.com/Pages/Documents/NLayer-Architecture)[^1], as well as several programming patterns.

### Application Design Patterns 

#### Inversion of Control (IoC)
The IoC pattern is used in this application to decouple the execution of tasks from their implementations via dependency injection.  

Classes are written depedent on interfaces instead of classes so that any class implementing the interface may be substituted at runtime as required.  This is allows us to mock implementations for performing unit testing.  It also provides for modularity, allowing us to change or modify implementaitons without having to update code that depends on them.  

BinaryFiles API uses Autofac as an IoC Container and dependency injector.  Each assembly has a class that extends the Autofac Module class, overriding a method that passes an intance of the container builder to it so it can register its service implementations.  Each module's concretions are registered during the applications startup routine.  

#### Repository
The repository pattern is used in this application to decouple the data domain from the data access layer.  

#### Unit of Work (UoW)
The uniit of work pattern is essentially a way of implementing a transaction within an `IRepository`.  Since the repository pattern intentionally abstracts datastore implementation details, a datastore-independement method of recovering from errors partway through an operation that modifies data is required.

#### Logging
While not a formal programming pattern, logging facilities are something every program should have.  This application makes use of Serilog which decouples log statements within the code from the log output, (which Serilog calls "appenders").  By using Serilog appenders, log messages can be appended to multiple outputs simultaneously, and appenders can filter the messages appended to them by log level.  

### Application Layers
The application layers are implemented in seperate class libaries (projects).  

* __BinaryFiles.API (Presentation Layer):__   
This layer is the interface, albeit not a graphical one, between consumers of the application and business logic encapsulated in the within it.  

* __BinaryFiles.Application (Application Layer):__   
This layer is the logical intermediary between the API layer and the domain layer.  It contains services and functions that call into the domain layer, executing domain logic and interacting domain data in response to API requests.  

* __BinaryFiles.Domain (Domain Layer):__   
The business domain is the set of objects that model the data structures and relationships that the business logic of the application operates on.  The domain layer contains entities, the set of classes that make up the domain model.  It also implements the application's business logic in the form of routines that operate upon instances of entities.  

* __BinaryFiles.EntityFramework (Infrastucture Layer):__   
This application as envisioned requires a data store, but there are many options.  Since this data layer is based on Entity Framework Core, it's name reflects its core dependendcy.  This leaves open the possibilty of adding libraries that support/leverage different ORMs, or possibly even NoSQL databases, in the future.  

* __BinaryFiles.DiskStorage (Infrastructure Layer):__   
The most obvious and straight forward way of saving binary data is directly to the local hard drive, but in a high-volume scenario, this implementation probably isn't feasible.  I believe file storage should be decoupled from the rest of the application to allow for the possibility of swapping the storage provider out for more appropriate solutions in the future.  

## Database notes

I picked Entity Framework Core (EF Core) as the ORM for this project because its code-first functionality makes it easy to set up local databases for demonstration and testing purposes.  

EF Core has an in-memory provider for Sqlite, so I opted for Sqlite over alternatives. The testing library uses in-memory Sqlite database, and the WebAPI application either uses an existing Sqlite database file, or creates a new one if the specified file doesn't exist.  In the future I would move the connection string into appsettings.json and make this aspect of the application configurable.

I chose to use fluent API instead of entity model annotations to keep database implementaiton details out of the domain entity models.  The exception is the [Required] annotation which expresses database-independent information about entity properties.  

##  Extras

Swagger/Swashbuckle middleware was included in the WebAPI application to make understanding and testing the available controller methods easy.

##  Final Thoughts

This application is incomplete, but I believe it fulfills the core requirements by making endpoints available to do basic CRUD operations on binary files over HTTP.  There are quite a few `//TODOs` scattered throughout the code base, and test coverage is far less than 100%.  There are a few business logic decision points I punted on for now, considering them to be nice-to-haves, but not critical.  However, I believe it shows what I'm capable of.  I've attempted to bring together several technologies, design patterns, open source libraries, and programing philosophies to demonstrate the breadth and depth of my skill, interest, and experience.


## References
https://docs.microsoft.com/en-us/aspnet/core/mvc/models/file-uploads?view=aspnetcore-3.1#storage-scenarios-1  
https://docs.microsoft.com/en-us/ef/core/modeling/value-conversions   
https://docs.microsoft.com/en-us/ef/core/querying/related-data/lazy  
https://docs.microsoft.com/en-us/aspnet/core/web-api/advanced/formatting?view=aspnetcore-3.1  
https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio  
https://www.codewithmukesh.com/blog/repository-pattern-in-aspnet-core/   
https://www.codeproject.com/Articles/5259677/Creating-Simple-Audit-Trail-With-Entity-Framework   
http://www.databaseanswers.org/data_models/doc_mgt_generic/index.htm   
https://www.entityframeworktutorial.net/efcore/changetracker-in-ef-core.aspx (defered on actually leveraging this)  
https://github.com/AshkanAbd/efCoreSoftDeletes  (ended up not implementing soft deletes at this time) 



## Endnotes
[^1]:  I linked to a document within a library/application framework called Asp.net Boilerplate because my own understanding and experience with the concepts and implementation of the DDD layered architecture are based on my experience with this project.  Of all the web-based DDD explainations, I find this documentation to be the easiest to digest.  While this application has no software dependencies on Asp.net Boilerplate, it _is_ inspired by it.
