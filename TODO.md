## TODOs

* [x] application service to save IFormFile
  * [x] domain service with unit of work to wrap the core file creation business logic
   	* [x] domain service to save File record  
   	* [x] domain service to save UserFileAccess record
   	* [x] domain service to save IFormFile stream
* [x] appliction service to get file stream 
  * [x] domain service with unit of work to wrap the core file selection business logic
    * [x] domain service to get UseFileAccess record
    * [x] domain service to get file stream for UserFileAccess.File.Url
* [x] application service to delete file
  * [x] domain service with unit of work to wrap the core file deletion business logic
    * [ ] domain service to mark File IsDeleted
* [x] application service to get list of File DTOs
  * [x] domain service to get files by user